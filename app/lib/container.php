<?php

namespace App\Lib;

class Container {

    protected static $container = array();

    public static function get($key)
    {
        if (isset(self::$container[$key])) {
            return self::$container[$key];
        } else {return null;}
    }

    public static function set($key, $value) {
        self::$container[$key] = $value;
    }

}