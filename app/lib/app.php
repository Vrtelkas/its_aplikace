<?php

namespace App\Lib;

use App\Lib\Router as Router;

use App\Application\Controllers;

class App {

    protected static $router;
    protected static $loader;

    public static function getRouter()
    {
        return self::$router;
    }

    public static function getLoader()
    {
        return self::$loader;
    }

    public static function run($uri, $loader)
    {
        self::$router = new Router($uri);
        self::$loader = $loader;

        $controllerClass = "App\\Application\\Controllers\\".self::$router->getController().'Controller';
        $controllerAction = strtolower(self::$router->getAction());

        $controller = new $controllerClass();

        if (method_exists($controller, $controllerAction)){
            $controller->$controllerAction();
        } else {
            throw new \Exception('Method '.$controllerAction.' of class '.$controllerClass.' does not exist');
        }

    }
}