<?php

namespace App\Lib;

use App\Lib\Container;


class Router {

    protected $uri;
    protected $controller;
    protected $action;
    protected $params;

    public function getUri()
    {
        return $this->uri;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getParams()
    {
        return $this->params;
    }


    public function __construct($uri)
    {
        $this->uri = urldecode(trim($uri, '/'));

        $this->controller = Container::get('defaultController');
        $this->action = Container::get('defaultAction');

        $uriParts = explode('?', $this->uri);
        $path = $uriParts[0];
        $pathParts = explode('/', $path);
        array_shift($pathParts);


        if (current($pathParts)) {
            $this->controller = ucfirst(current($pathParts));
            array_shift($pathParts);
        }

        if (current($pathParts)) {
            $this->action = current($pathParts);
            array_shift($pathParts);
        }

        $this->params = $pathParts;
    }

}