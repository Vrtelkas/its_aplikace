<?php

namespace App\Lib;

use App\Lib\App as App;

class Controller {

    protected $data;
    protected $model;
    protected $params;
    protected $twig;

    public function getData() {
        return $this->data;
    }

    public function getModel() {
        return $this->model;
    }

    public function getParams() {
        return $this->params;
    }

    public function __construct($data = array()) {
        $this->data = $data;
        $this->params = App::getRouter()->getParams();

        $this->twig = new \Twig_Environment(App::getLoader());
    }
}