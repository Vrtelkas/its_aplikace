<?php

namespace App\Application\Models;


class DataHandler {

    protected $database;

    public function __construct(\PDO $connection) {
        $this->database = $connection;
    }

    public function getArticles($articleId = null)
    {
        if (!isset($articleId)) {
            $result = $this->database->query('SELECT Text FROM articles', \PDO::FETCH_ASSOC);

            $articles = array();

            foreach($result as $row) {
                $articles[] = $row['Text'];
            }

            return $articles;
        } else {
            $statement = $this->database->prepare('SELECT Text FROM articles WHERE ID = ?');

            $statement->execute([
                $articleId
            ]);

            $result = $statement->fetch(\PDO::FETCH_ASSOC);

            $article = $result['Text'];
            return $article;
        }
    }
}
