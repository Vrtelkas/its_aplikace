<?php

namespace App\Application\Models;

use PHPMailer;
use App\Lib\Container;

class MailSender {

    protected $to;
    protected $subject;
    protected $message;

    public function __construct(array $data) {
        $this->to = $data['to'];
        $this->subject = $data['subject'];
        $this->message = $data['message'];
    }

    public function send()
    {
        $mail = new PHPMailer();

        $mail->isSMTP();


        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = Container::get('address');
        $mail->Password = Container::get('password');
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->setFrom(Container::get('address'));
        $mail->addAddress($this->to);

        $mail->isHTML(true);

        $mail->Subject = $this->subject;
        $mail->Body = $this->message;

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }
}