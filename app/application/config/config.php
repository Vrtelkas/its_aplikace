<?php
/**
 * Created by PhpStorm.
 * User: VRTAK
 * Date: 19.07.2016
 * Time: 20:28
 */

namespace App\Application\Config;

use App\Lib\Container;


Container::set('defaultController', 'Home');
Container::set('defaultAction', 'index');


Container::set('connection', new \PDO('mysql:host=db4free.net;dbname=its_aplikace', 'vrtelkas', 'heslo123'));

Container::set('address', 'email@example.com');
Container::set('password', 'password');