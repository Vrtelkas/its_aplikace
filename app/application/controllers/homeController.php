<?php

namespace App\Application\Controllers;

use App\Lib\Controller as Controller;
use App\Lib\App as App;
use App\Application\Models\DataHandler as DataHandler;
use App\Lib\Container as Container;
use App\Application\Models\MailSender as MailSender;


class HomeController extends Controller {


    public function index() {
        echo $this->twig->render('homepage.html', array(
            "" => ""
        ));
    }

    public function articles() {

        $params = App::getRouter()->getParams();
        $handler = new DataHandler(Container::get('connection'));

        if (isset($params[0])) {
            $articles = $handler->getArticles($params['0']);
        } else {
            $articles = $handler->getArticles();
        }

        $cancer = "supercancer";

        echo $this->twig->render('articles.html', array(
            "articles" => $articles
        ));

    }

    public function send()
    {
        $data = array(
            "to" => $_POST['to'],
            "subject" => $_POST['subject'],
            "message" => $_POST['message']
        );

        $sender = new MailSender($data);

        $sender->send();
    }
}