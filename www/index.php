<?php

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../app/lib/app.php';

require __DIR__ . '/../app/application/config/config.php';

use App\Lib\App as App;

$lodaer = new Twig_Loader_Filesystem('../app/application/views');

App::run($_SERVER['REQUEST_URI'], $lodaer);





